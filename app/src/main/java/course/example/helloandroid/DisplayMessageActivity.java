package course.example.helloandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        //vista para el texto
        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(message);
        //vista del texto como el layout de la actividad
        setContentView(textView);

    }
}
